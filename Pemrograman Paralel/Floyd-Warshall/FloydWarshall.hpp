//file		:	FloydWarshall.hpp
//author	:	steaKK

#ifndef FloydWarshall_HPP
#define FloydWarshall_HPP

#include <iostream>
#include <omp.h>

#include "Matrix.hpp"

using namespace std;

class FloydWarshall {
public:
	FloydWarshall();
	FloydWarshall(int,int);
	FloydWarshall(const FloydWarshall&);
	FloydWarshall& operator=(const FloydWarshall&);
	~FloydWarshall();

	Matrix get_data();
	void set_data(Matrix);

	void print();

	Matrix solve_sequential();
	Matrix solve_openmp();

private:
	static const int DEFAULT_NODE_SIZE = 4;
	Matrix data;

};

#endif
