//file		:	main.cpp
//author	:	steaKK

#include "BitString.hpp"

int main() {
	BitString b0;
	BitString b1(64);
	BitString b2(b1);
	BitString b3 = b2;

	b3.print();

	b3.set_size(20);
	cout << "size = " << b3.get_size() << endl;
	b3.set_data(0,true);
	cout << "data ke-0 = " << b3.get_data(0) << endl;

	b3.print();

	return 0;
}
