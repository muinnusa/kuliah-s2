//file		:	main.cpp
//author	:	steaKK

#include "NthPrime.h"
#include <ctime>

int main() {
	NthPrime n0(100);
	NthPrime n1(100);
	NthPrime n2(100);

	time_t t0 = time(NULL);
	cout << "(sequential)     prime ke-" << n0.getnth() << " = " << n0.solve_sequential() << endl;
	for(int i=0;i<n1.getnth();i++) {
		cout << n0.getdata(i) << " ";
	}
	cout << endl;
	time_t t1 = time(NULL);
	cout << "selesai dalam " << difftime(t1,t0) << " detik" << endl;

	cout << "(openmp) prime ke-" << n1.getnth() << " = " << n1.solve_parallel() << endl;
	for(int i=0;i<n1.getnth();i++) {
		cout << n1.getdata(i) << " ";
	}
	cout << endl;
	time_t t2 = time(NULL);
	cout << "selesai dalam " << difftime(t2,t1) << " detik" << endl;

	cout << "(cilk) prime ke-" << n2.getnth() << " = " << n2.solve_cilk() << endl;
	for(int i=0;i<n1.getnth();i++) {
		cout << n1.getdata(i) << " ";
	}
	cout << endl;
	time_t t3 = time(NULL);
	cout << "selesai dalam " << difftime(t3,t2) << " detik" << endl;

	return 0;
}
