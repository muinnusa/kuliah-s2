//file		:	BitString.cpp
//author	:	steaKK

#include "BitString.hpp"

BitString::BitString() {
	size = DEFAULT_SIZE;
	data = new bool[size];
	for(int i=0;i<size;i++) data[i] = false;
}

BitString::BitString(int _size) {
	size = _size;
	data = new bool[size];
	for(int i=0;i<size;i++) data[i] = false;
}
BitString::BitString(const BitString& _BitString) {
	size = _BitString.size;
	data = new bool[size];
	for(int i=0;i<size;i++) data[i] = _BitString.data[i];
}

BitString& BitString::operator=(const BitString& _BitString) {
	size = _BitString.size;
	for(int i=0;i<size;i++) data[i] = _BitString.data[i];
	return *this;
}

BitString::~BitString() {
	delete[] data;
}

int BitString::get_size() {
	return size;
}
void BitString::set_size(int _size) {
	size = _size;
}

bool BitString::get_data(int pos) {
	return data[pos];
}

void BitString::set_data(int pos, bool val) {
	data[pos] = val;
}

void BitString::print() {
	for(int i=0;i<size;i++) cout << data[i] << " ";
	cout << endl;
}

void BitString::shift_right() {
	bool temp = data[0];
	for(int i=0;i<size-1;i++) data[i] = data[i+1];
	data[size-1] = temp;
}

void BitString::shift_left() {
	bool temp = data[size-1];
	for(int i=0;i<size-1;i++) data[i+1] = data[i];
	data[0] = temp;
}

void BitString::shift_n_right(int x) {
	for(int i=0;i<x;i++) shift_right;
}

void BitString::shift_n_left(int x) {
	for(int i=0;i<x;i++) shift_left;
}


BitString BitString::concat(BitString b1, BitString b2) {
	BitString result(b1.size()+b2.size());
	int pos = 0;
	for(int i=0;i<b1.size();i++) {
		result[pos] = b1[i];
		pos++;
	}
	for(int i=0;i<b2.size();i++) {
		result[pos] = b1[i];
		pos++;
	}
	return result;
}

BitString BitString::xor(BitString b1, BitString b2) {
	BitString result(b1.size());
	for(int i=0;i<b1.size();i++) {
		result.set_data(i,(b1.get_data(i)+b2.get_data(i))%2);
	}
	return result;
}
