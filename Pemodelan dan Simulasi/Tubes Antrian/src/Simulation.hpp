//file		:	Simulation.hpp
//author	:	steaKK

#ifndef Simulation_HPP
#define Simulation_HPP

#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

#include "Machine.hpp"

using namespace std;

class Simulation {
public:
	Simulation();
	Simulation(int,int,int,vector<Machine>);
	Simulation(const Simulation&);
	Simulation& operator=(const Simulation&);
	~Simulation();

	int get_start();
	int get_end();
	int get_now();
	void set_now(int);
	vector<Machine> get_data();
	void set_data(vector<Machine>);

	void print();
	void add_batch_random(int,int,int);

	bool is_timeup();
	int get_next_job();
	void update_front(int);
	void step();
	int get_shortest_machine();
	bool is_done();
	void proccess_normal();

private:
	int start;
	int end;
	int now;
	vector<Machine> data;
};

#endif
