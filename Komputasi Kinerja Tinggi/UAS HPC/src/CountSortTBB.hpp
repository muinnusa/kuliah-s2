//file		:	CountSortTBB.hpp
//author	:	steaKK

#ifndef countBody_HPP
#define countBody_HPP

#include <tbb/tbb.h>

using namespace tbb;

class CountSortTBB{
public:
	CountSortTBB(int* _data) : data(_data) {

	}

	void operator() ( const blocked_range <int>& br) const {
		int *temp = new int[br.end()];
		for (int i=br.begin();i!=br.end();i++){
			int count = 0;
			for (int j=br.begin();j!=br.end();j++){
				if (data[j]<data[i]) count++;
				else if (data[j] == data[i] && j<i) count++;
				}
			temp[count] = data[i];
		}
		memcpy(data, temp, br.end() * sizeof(int));
		delete[] temp;
	}

private:
	int* data;
};

#endif
