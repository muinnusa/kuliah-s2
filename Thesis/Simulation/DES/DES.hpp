//file		:	DES.hpp
//author	:	steaKK

#ifndef DES_HPP
#define DES_HPP

#include <iostream>
#include <vector>
#include <fstream>

#include "Matrix.hpp"
#include "BitString"

using namespace std;

class DES {
public:
	DES();
	~DES();

	void print_table();

	BitString pc1(bool,Bitstring);
	BitString pc2(Bitstring);

private:
	static const int permutation_i_size = 64;
	static const int permutation_f_size = 64;
	static const int expansion_size = 48;
	static const int permutation_p_size = 32;
	static const int permutation_pc1_size = 28;
	static const int permutation_pc2_sizeE = 48;
	static const int sboxes_size = 8;
	static const int rotation_key_size = 16;

	int* permutation_i;
	int* permutation_f;
	int* expansion;
	int* permutation_p;
	int* permutation_pc1_l;
	int* permutation_pc1_r;
	int* permutation_pc2;
	Matrix* sboxes;
	int* rotation_key;
};

#endif
