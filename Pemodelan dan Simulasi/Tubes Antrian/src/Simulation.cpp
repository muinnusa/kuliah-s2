//file		:	Simulation.cpp
//author	:	steaKK

#include "Simulation.hpp"

Simulation::Simulation() {

}

Simulation::Simulation(int _start, int _end, int _now, vector<Machine> _data) {
	start = _start;
	end = _end;
	now = _now;
	data = _data;
}

Simulation::Simulation(const Simulation& _Simulation) {
	start = _Simulation.start;
	end = _Simulation.end;
	now = _Simulation.now;
	data = _Simulation.data;
}

Simulation& Simulation::operator=(const Simulation& _Simulation) {
	start = _Simulation.start;
	end = _Simulation.end;
	now = _Simulation.now;
	data = _Simulation.data;
	return *this;
}

Simulation::~Simulation() {

}

int Simulation::get_start() {
	return start;
}

int Simulation::get_end() {
	return end;
}

int Simulation::get_now() {
	return now;
}

void Simulation::set_now(int _now) {
	now = _now;
}

vector<Machine> Simulation::get_data() {
	return data;
}

void Simulation::set_data(vector<Machine> _data) {
	data = _data;
}

void Simulation::print() {
	for(int i=0;i<data.size();i++) {
		cout << "Machine ke-" << i << " ";
		data[i].print();
	}
}

void Simulation::add_batch_random(int duration, int chance_come, int chance_duration) {
	srand(time(NULL));
	vector<Job> tab;
	for(int i=0;i<duration;i++) {
		int roll_nJob = rand()%chance_come;
		for(int j=0;j<roll_nJob;j++) {
			int roll_duration = rand()%chance_duration + 1;
			Job temp(roll_duration);
			tab.push_back(temp);
		}
	}
	for(int i=0;i<tab.size();i++) {
		data[get_shortest_machine()].get_data().push_back(tab[i]);
	}
}

bool Simulation::is_timeup() {
	if(now<end) return false;
	else return true;
}

int Simulation::get_next_job() {
	int result = 999;
	for(int i=0;i<data.size();i++) {
		if(data[i].get_data().front().get_duration()<result) result = i;
	}
	return result;
}

int Simulation::get_shortest_machine() {
	int result = 999;
	for(int i=0;i<data.size();i++) {
		if(result>data[i].get_data().size()) result = i;
	}
	return result;
}

bool Simulation::is_done() {
	for(int i=0;i<data.size();i++) {
		if(!data[i].get_data().empty()) return false;
	}
	return true;
}

void Simulation::update_front(int duration) {
	for(int i=0;i<data.size();i++) {
		data[i].get_data().front().set_duration(data[i].get_data().front().get_duration()-duration);
	}
}

void Simulation::step() {
	int id = get_next_job();
	int duration = data[id].get_data().front().get_duration();
	now += duration;
	update_front(duration);
	if(!is_timeup()) add_batch_random(duration,2,5);
	data[id].get_data().pop_back();
}

void Simulation::proccess_normal() {
	add_batch_random(10,2,5);
	while(is_timeup() || is_done()) {
		step();
		print();
	}
}
