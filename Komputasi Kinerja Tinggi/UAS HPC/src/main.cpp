//file		:	main.cpp
//author	:	steaKK

#include "CountSort.h"

int main() {

	//initialization
	long n0 = 16;
	int* data0 = new int[n0];
	long n1 = 256;
	int* data1 = new int[n1];
	long n2 = 4096;
	int* data2 = new int[n2];
	long n3 = 65536;
	int* data3 = new int[n3];
	long n4 = 1048576;
	int* data4 = new int[n4];
	// long n5 = 4194304;
	// int* data6 = new int[n5];

	//openmp
	CountSort::randomize_data(data0,n0);
	time_t t0 = time(NULL);
	CountSort::sort_openmp(data0,n0);
	time_t t1 = time(NULL);
	cout << "OPENMP with " << n0 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data1,n1);
	t0 = time(NULL);
	CountSort::sort_openmp(data1,n1);
	t1 = time(NULL);
	cout << "OPENMP with " << n1 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data2,n2);
	t0 = time(NULL);
	CountSort::sort_openmp(data2,n2);
	t1 = time(NULL);
	cout << "OPENMP with " << n2 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data3,n3);
	t0 = time(NULL);
	CountSort::sort_openmp(data3,n3);
	t1 = time(NULL);
	cout << "OPENMP with " << n3 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data4,n4);
	t0 = time(NULL);
	CountSort::sort_openmp(data4,n4);
	t1 = time(NULL);
	cout << "OPENMP with " << n4 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	// CountSort::randomize_data(data5,n5);
	// t0 = time(NULL);
	// CountSort::sort_openmp(data5,n5);
	// t1 = time(NULL);
	// cout << "OPENMP with " << n5 << " data = " << difftime(t1,t0) << " second(s)" << endl;

	//tbb
	CountSort::randomize_data(data0,n0);
	t0 = time(NULL);
	CountSort::sort_tbb(data0,n0);
	t1 = time(NULL);
	cout << "TBB with " << n0 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data1,n1);
	t0 = time(NULL);
	CountSort::sort_tbb(data1,n1);
	t1 = time(NULL);
	cout << "TBB with " << n1 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data2,n2);
	t0 = time(NULL);
	CountSort::sort_tbb(data2,n2);
	t1 = time(NULL);
	cout << "TBB with " << n2 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data3,n3);
	t0 = time(NULL);
	CountSort::sort_tbb(data3,n3);
	t1 = time(NULL);
	cout << "TBB with " << n3 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data4,n4);
	t0 = time(NULL);
	CountSort::sort_tbb(data4,n4);
	t1 = time(NULL);
	cout << "TBB with " << n4 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	// CountSort::randomize_data(data5,n5);
	// t0 = time(NULL);
	// CountSort::sort_tbb(data5,n5);
	// t1 = time(NULL);
	// cout << "TBB with " << n5 << " data = " << difftime(t1,t0) << " second(s)" << endl;

	//cilk
	CountSort::randomize_data(data0,n0);
	t0 = time(NULL);
	CountSort::sort_cilk(data0,n0);
	t1 = time(NULL);
	cout << "CILK with " << n0 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data1,n1);
	t0 = time(NULL);
	CountSort::sort_cilk(data1,n1);
	t1 = time(NULL);
	cout << "CILK with " << n1 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data2,n2);
	t0 = time(NULL);
	CountSort::sort_cilk(data2,n2);
	t1 = time(NULL);
	cout << "CILK with " << n2 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data3,n3);
	t0 = time(NULL);
	CountSort::sort_cilk(data3,n3);
	t1 = time(NULL);
	cout << "CILK with " << n3 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	CountSort::randomize_data(data4,n4);
	t0 = time(NULL);
	CountSort::sort_cilk(data4,n4);
	t1 = time(NULL);
	cout << "CILK with " << n4 << " data = " << difftime(t1,t0) << " second(s)" << endl;
	// CountSort::randomize_data(data5,n5);
	// t0 = time(NULL);
	// CountSort::sort_cilk(data5,n5);
	// t1 = time(NULL);
	// cout << "CILK with " << n5 << " data = " << difftime(t1,t0) << " second(s)" << endl;

	//finalization
	delete[] data0;
	delete[] data1;
	delete[] data2;
	delete[] data3;
	delete[] data4;
	// delete[] data5;

	return 0;
}
