//file		:	CountSort.h
//author	:	steaKK

#ifndef CountSort_H
#define CountSort_H

#include <iostream>
#include <cstring>
#include <cstdlib>
#include <ctime>

#include <omp.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

#include "CountSortTBB.hpp"

using namespace std;

class CountSort {
public:
	static void sort(int*,long);
	static void sort_openmp(int*,long);
	static void sort_tbb(int*,long);
	static void sort_cilk(int*,long);

	static void randomize_data(int*,long);
	static void print(int*,long);
private:
	static const int idealGrainSize = 100;
};

#endif
