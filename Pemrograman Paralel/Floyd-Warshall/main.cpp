//file		:	main.cpp
//author	:	steaKK

#include "FloydWarshall.hpp"

int main() {
	FloydWarshall f0;
	FloydWarshall f1(5,5);
	FloydWarshall f2(f1);

	Matrix m1(5,5);
	for(int i=0;i<m1.get_height();i++) {
		for(int j=0;j<m1.get_width();j++) {
			m1.set_data(i,j,1);
		}
	}
	f2.set_data(m1);

	FloydWarshall f3 = f2;

	f3.print();

	cout << endl;
	f3.solve_sequential().print();
	cout << endl;
	f3.solve_openmp().print();

	return 0;
}
