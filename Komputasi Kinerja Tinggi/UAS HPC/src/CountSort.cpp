//file		:	CountSort.h
//author	:	steaKK

#include "CountSort.h"

void CountSort::sort(int* data, long n) {
	int* temp = new int[n];
	for(long i=0;i<n;i++) {
		int count = 0;
		for(long j=0;j<n;j++) {
			if(data[j]<data[i]) count++;
			else if(data[j] == data[i] && j<i) count++;
		}
		temp[count] = data[i];
		// cout << "i = " << i << endl;
		// cout << "count = " << count << endl;
		// cout << "temp[count] = " << temp[count] << endl;
		// cout << "data[i] = " << data[i] << endl;
		// cout << endl;
	}
	memcpy(data,temp,n*sizeof(int));
	delete[] temp;
}

void CountSort::sort_openmp(int* data, long n) {
	int* temp = new int[n];
	omp_set_num_threads(omp_get_num_procs());
	#pragma omp parallel for
	for(long i=0;i<n;i++) {
		long count = 0;
		#pragma omp parallel for
		for(long j=0;j<n;j++) {
			if(data[j]<data[i]) count++;
			else if(data[j] == data[i] && j<i) count++;
		}
		temp[count] = data[i];
	}
	memcpy(data,temp,n*sizeof(int));
	delete[] temp;
}

void CountSort::sort_tbb(int* data, long n) {
	CountSortTBB temp(data);
	parallel_for (blocked_range <int>(0, n, idealGrainSize),temp);
}

void CountSort::sort_cilk(int* data, long n) {
	int* temp = new int[n];
	cilk_for(long i=0;i<n;i++) {
		long count = 0;
		for(long j=0;j<n;j++) {
			if(data[j]<data[i]) count++;
			else if(data[j] == data[i] && j<i) count++;
		}
		temp[count] = data[i];
	}
	memcpy(data,temp,n*sizeof(int));
	delete[] temp;
}

void CountSort::randomize_data(int* data, long n) {
	srand (time(NULL));
	for(int i=0;i<n;i++) {
		data[i] = rand() % 1000;
	}
}

void CountSort::print(int* data, long n) {
	for(long i=0;i<n;i++) {
		cout << "[" << data[i] << "]";
	}
	cout << endl;
}
