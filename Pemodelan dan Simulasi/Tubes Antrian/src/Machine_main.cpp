//file		:	Machine_main.cpp
//author	:	steaKK

#include "Machine.hpp"

int main() {
	Job j0;
	Job j1(10);
	Job j2(j1);
	Job j3 = j2;
	Job j4(30);

	vector<Job> tab;
	tab.push_back(j0);
	tab.push_back(j1);
	tab.push_back(j2);
	tab.push_back(j3);

	Machine m0;
	Machine m1(tab);
	Machine m2(m1);
	Machine m3 = m2;

	tab.push_back(j4);

	m3.get_data()[1].print();
	m3.set_data(tab);

	m3.print();

	return 0;
}
