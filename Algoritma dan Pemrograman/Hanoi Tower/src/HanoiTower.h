//file      :   HanoiTower.h
//author    :   steaKK

#ifndef HanoiTower_H
#define HanoiTower_H

#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

class HanoiTower {
public :
    //gangof5
    HanoiTower();
    HanoiTower(int);
    HanoiTower(const HanoiTower&);
    HanoiTower& operator=(const HanoiTower&);
    ~HanoiTower();

    //operator overloading
    bool operator==(const HanoiTower&);

    //tester
    void print();
    void init();

    //gettersetter
    int get_npole();
    int get_data(int);
    void set_npole(int);
    void set_data(int,int);

    //method
    bool is_equal(HanoiTower);
    bool is_poleempty(int);
    bool is_atobavailable(int,int);
    bool is_atobavailable_t1(int,int);
    bool is_done();

    void move_atob(int,int);

private :
    //constant
    static const int DEFAULT_NPOLE = 3;

    //attribute
    int npole;
    vector<vector<int> > data;
};

#endif
