//file		:	Matrix.hpp
//author	:	steaKK

#ifndef Matrix_HPP
#define Matrix_HPP

#include <iostream>

using namespace std;

class Matrix {

public:
	Matrix();
	Matrix(int,int);
	Matrix(const Matrix&);
	Matrix& operator=(const Matrix&);
	~Matrix();

	int get_height();
	void set_height(int);
	int get_width();
	void set_width(int);
	int get_data(int,int);
	void set_data(int,int,int);

	void print();

private:
	static const int DEFAULT_HEIGHT = 4;
	static const int DEFAULT_WIDTH = 4;

	int height;
	int width;
	int* data;

};

#endif
